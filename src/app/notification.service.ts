import { Injectable } from '@angular/core';
import { Firebase } from "@ionic-native/firebase/ngx";
import { AngularFirestore } from "angularfire2/firestore";

@Injectable({
  providedIn: "root"
})
export class NotificationService {
  constructor(public firebase: Firebase, public firestore: AngularFirestore) {}

  async getTokens() {
    const token = await this.firebase.getToken();
    this.saveToken(token);
  }

  saveToken(token) {
    const ref = this.firestore.collection('devices');
    ref.doc(token).set({
      token,
      userId: 'test'
    });
  }

  onNotifications() {
    return this.firebase.onNotificationOpen();
  }
}

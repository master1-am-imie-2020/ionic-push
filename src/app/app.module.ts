import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from 'angularfire2';
import { Firebase } from '@ionic-native/firebase/ngx';
import { AngularFirestoreModule } from 'angularfire2/firestore';

  const config = {
    apiKey: "AIzaSyB3rtgVSp7duPxHuGHtBGNRK7CJuEE2754",
    authDomain: "push-master-test-33e62.firebaseapp.com",
    databaseURL: "https://push-master-test-33e62.firebaseio.com",
    projectId: "push-master-test-33e62",
    storageBucket: "push-master-test-33e62.appspot.com",
    messagingSenderId: "1035732070781"
  };

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IonicModule.forRoot(),
    AngularFirestoreModule,
    AngularFireModule.initializeApp(config)
  ],
  providers: [
    Firebase,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
